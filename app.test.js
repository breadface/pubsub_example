const axios = require("axios");
const baseUrl = "http://localhost:8000";

jest.mock("axios");

test("Should subscribe to a topic", async () => {
  const data = { url: "http://localhost:8000/event" };
  axios.post.mockImplementationOnce(() => Promise.resolve(data));

  await expect(
    axios.post(`${baseUrl}/subscribe/writing`, data)
  ).resolves.toEqual(data);
  expect(axios.post).toHaveBeenCalledWith(`${baseUrl}/subscribe/writing`, data);
});

test("Should publish to a subscribed topic", async () => {
  axios.post.mockImplementationOnce(() =>
    Promise.resolve({ msg: `Topic writing has been published.` })
  );

  await expect(
    axios.post(`${baseUrl}/publish/writing`, { msg: "Hello world" })
  ).resolves.toEqual({ msg: `Topic writing has been published.` });
});
