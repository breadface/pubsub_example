### Setup

- Run `yarn add` or `npm install` to install packages
- Make an executable bash file using `chmod +x start-server.sh`
- Run server with `./start-server`
