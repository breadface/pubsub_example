const Express = require("express");
const bodyParser = require("body-parser");
const axios = require("axios");
const PORT = 8000;

const app = Express();
app.use(bodyParser.json());

const events = {};

app.post("/subscribe/:topic", (req, res) => {
  const { topic } = req.params;
  const { url } = req.body;

  events[topic] = (events[topic] || new Set()).add(url);
  res.json(req.body);
});

app.post("/publish/:topic", async (req, res) => {
  const { topic } = req.params;
  const listeners = events[topic];

  if (listeners) {
    await axios.all(
      [...listeners].map(url => axios({ url, method: "post", data: req.body }))
    );
    res.json({ msg: `Topic ${topic} has been published.` });
  } else {
    res.status(404).json({ error: `Topic ${topic} doesn't exist` });
  }
});

app.post("/event", (req, res) => {
  console.log(req.body);
  res.send(req.body);
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT} ...`);
});
